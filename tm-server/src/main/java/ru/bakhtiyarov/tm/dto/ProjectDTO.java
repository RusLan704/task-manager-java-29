package ru.bakhtiyarov.tm.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    @NotNull
    public ProjectDTO(@NotNull String name, @Nullable String description, @Nullable String userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @Nullable String userId) {
        this.name = name;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}

package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.Role;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void checkRole(@Nullable Role[] roles);

    void logout();

    void login(@Nullable String login,@Nullable String password);

    void registry(@Nullable String login,@Nullable String password,@Nullable String email);

}

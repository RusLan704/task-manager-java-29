package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @NotNull
    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}

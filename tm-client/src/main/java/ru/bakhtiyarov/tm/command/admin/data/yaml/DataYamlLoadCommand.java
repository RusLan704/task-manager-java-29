package ru.bakhtiyarov.tm.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

public final class DataYamlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from YAML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML LOAD]");
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        endpointLocator.getAdminDataEndpoint().loadYaml(session);
        System.out.println("[OK]");
    }

}


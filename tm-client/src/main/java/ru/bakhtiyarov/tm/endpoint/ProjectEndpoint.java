package ru.bakhtiyarov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-06T01:23:57.290+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByIndexRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByIndexResponse")
    @RequestWrapper(localName = "findProjectOneByIndex", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneByIndex")
    @ResponseWrapper(localName = "findProjectOneByIndexResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO findProjectOneByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        int index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/createProjectByNameDescriptionRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/createProjectByNameDescriptionResponse")
    @RequestWrapper(localName = "createProjectByNameDescription", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateProjectByNameDescription")
    @ResponseWrapper(localName = "createProjectByNameDescriptionResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateProjectByNameDescriptionResponse")
    public void createProjectByNameDescription(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByIndexRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByIndexResponse")
    @RequestWrapper(localName = "removeProjectOneByIndex", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneByIndex")
    @ResponseWrapper(localName = "removeProjectOneByIndexResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO removeProjectOneByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        int index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/createProjectByNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/createProjectByNameResponse")
    @RequestWrapper(localName = "createProjectByName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateProjectByName")
    @ResponseWrapper(localName = "createProjectByNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateProjectByNameResponse")
    public void createProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO updateProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByNameResponse")
    @RequestWrapper(localName = "findProjectOneByName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneByName")
    @ResponseWrapper(localName = "findProjectOneByNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO findProjectOneByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByIdRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByIdResponse")
    @RequestWrapper(localName = "removeProjectOneById", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneById")
    @ResponseWrapper(localName = "removeProjectOneByIdResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO removeProjectOneById(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO updateProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        int index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByIdRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findProjectOneByIdResponse")
    @RequestWrapper(localName = "findProjectOneById", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneById")
    @ResponseWrapper(localName = "findProjectOneByIdResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindProjectOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO findProjectOneById(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findAllProjectsBySessionRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/findAllProjectsBySessionResponse")
    @RequestWrapper(localName = "findAllProjectsBySession", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindAllProjectsBySession")
    @ResponseWrapper(localName = "findAllProjectsBySessionResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindAllProjectsBySessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.bakhtiyarov.tm.endpoint.ProjectDTO> findAllProjectsBySession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeAllProjectsBySessionRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeAllProjectsBySessionResponse")
    @RequestWrapper(localName = "removeAllProjectsBySession", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveAllProjectsBySession")
    @ResponseWrapper(localName = "removeAllProjectsBySessionResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveAllProjectsBySessionResponse")
    public void removeAllProjectsBySession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/ProjectEndpoint/removeProjectOneByNameResponse")
    @RequestWrapper(localName = "removeProjectOneByName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneByName")
    @ResponseWrapper(localName = "removeProjectOneByNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.RemoveProjectOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.ProjectDTO removeProjectOneByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );
}

package ru.bakhtiyarov.tm;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;

public class Client {

    public static void main(String... args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}


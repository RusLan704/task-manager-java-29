package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E persist(@NotNull E record);

    @NotNull
    E merge(@NotNull final E e);

    @Nullable
    E remove(@NotNull E record);

    void addAll(@NotNull List<E> records);

}
